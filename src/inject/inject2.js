chrome.extension.sendMessage({}, function(response) {
	var readyStateCheckInterval = setInterval(function() {
	if (document.readyState === "complete") {
		clearInterval(readyStateCheckInterval);

		// ----------------------------------------------------------
		// This part of the script triggers when page is done loading
		console.log("Hello. This message was sent from scripts/inject.js");
		//alert('done!');

		
		function injectScript(file, node) {
		    var th = document.getElementsByTagName(node)[0];
		    var s = document.createElement('script');
		    s.setAttribute('type', 'text/javascript');
		    s.setAttribute('src', file);
		    th.appendChild(s);
		}
		injectScript( chrome.extension.getURL('src/inject/dom.js'), 'body');
		// ----------------------------------------------------------

	}
	}, 10);
});