var readyStateCheckInterval = setInterval(function() {
	if (typeof jQuery != 'undefined') {
		clearInterval(readyStateCheckInterval);
		$('.text11').each(function() {
			var out = $.trim($(this).text());
			if (matches = out.match(/(\d{2})\:(\d{2}) - (\d{2}):(\d{2})/i)) {
				var time_end = (parseInt(matches[3])*60)+parseInt(matches[4]);
				var time_start = (parseInt(matches[1])*60)+parseInt(matches[2]);
				var diff = time_end - time_start - 60;
				var hours = Math.floor(diff / 60);
				var mins = diff % 60;
				var time = ('0' + hours).slice(-2) + ":" + ('0' + mins).slice(-2);
				if (diff > 0) {
					$(this).css("font-size", "110%");
					$(this).append('<br /><span style="background:'+(diff >= 60*8 ? 'green' : 'red')+';color:#fff;float:left;"><input type="checkbox" value="'+diff+'" id="checktime"> ' + time + ' </span><div style="clear:both;">&nbsp;</div>');
				}

				
			}
		});

		var hour_select_list = '';
		var min_select_list = '';
		for (i=5;i<=22;i++) {
			hour_select_list += '<option>'+i+'</option>';
		}
		for (i=0;i<=59;i++) {
			min_select_list += '<option>'+i+'</option>';
		}

		$('#divBody').before('<span style="top:0px;position:fixed;top:0px;right:0px;padding:20px;background:#ccc;"><input type="checkbox" id="caltime"> เพิ่มเวลา<span id="caltime_option" style="display:none;"><br />เข้า <select id="start_hour">'+hour_select_list+'</select> : <select id="start_min">'+min_select_list+'</select><br />ออก <select id="end_hour">'+hour_select_list+'</select> : <select id="end_min">'+min_select_list+'</select></span><hr /><span id="total_time" style="font-size:200%;"></span></span>');

		$(':checkbox[id=caltime]').change(function() {
			if (this.checked) {
				$('#caltime_option').show();
			} else {
				$('#caltime_option').hide();
			}
			calTime();
		});
		$(':checkbox[id=checktime], #start_hour, #end_hour, #start_min, #end_min').change(function() {
			calTime();
		});
		function calTime() {
			var total = 0;
			if ($('#caltime_option').is(':visible')) {
				time_start = parseInt($('#start_hour').val())*60 + parseInt($('#start_min').val());
				time_end = parseInt($('#end_hour').val())*60 + parseInt($('#end_min').val());
				diff = time_end - time_start - 60;
				if (diff > 0) {
					total += diff;
				}
			}
			$(':checkbox[id=checktime]:checked').each(function() {
				total += parseInt($(this).val());
			});
			var hours = Math.floor(total / 60);
			var mins = total % 60;
			var time = ('0' + hours).slice(-2) + ":" + ('0' + mins).slice(-2);
			if (total < 40*60) {
				var diff = 40*60 - total;
				var hours_diff = Math.floor(diff / 60);
				var mins_diff = diff % 60;
				var time_diff = ('0' + hours_diff).slice(-2) + ":" + ('0' + mins_diff).slice(-2);
				$('#total_time').html('เวลารวม ' + time + '<br />ขาดอีก ' + time_diff);
			} else {
				$('#total_time').html('เวลารวม ' + time);
			}
		}
	}
}, 100);