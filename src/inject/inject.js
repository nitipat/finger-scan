chrome.extension.sendMessage({}, function(response) {
	var readyStateCheckInterval = setInterval(function() {
	if (document.readyState === "complete") {
		clearInterval(readyStateCheckInterval);
		
		function injectScript(file, node) {
		    var th = document.getElementsByTagName(node)[0];
		    var s = document.createElement('script');
		    s.setAttribute('type', 'text/javascript');
		    s.setAttribute('src', file);
		    th.appendChild(s);
		}

		injectScript( chrome.extension.getURL('js/jquery/jquery.min.js'), 'body');
		window.setTimeout(function() {
			if (document.domain == 'hris.thinknet.co.th') {
				injectScript( chrome.extension.getURL('src/inject/dom_fingure.js'), 'body');
			} else {
				injectScript( chrome.extension.getURL('src/inject/dom_main.js'), 'body');		
			}
		}, 500);
		

	}
	}, 100);
});